# Executive Summary

## Nama Telegram

[![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/Uncle_Adprog_Bot)

Nama bot di Telegram: UncleAdprog

## Daftar Fitur

### Announcement 

Fitur ini dapat digunakan oleh penggunan untuk mengambil (hingga) 5 pengumuman terakhir di SCELE.

### Jadwal

Fitur ini mengakses jadwal 6 hari kuliah mahasiswa dari api.cs.ui jika diberikan parameter NPM.

### Waktu

Fitur ini memberikan jam scele berdasarkan ntp.ui.ac.id.

### Weather

Fitur ini mengakses keadaan cuaca sekarang untuk UI Depok dan UI Salemba dari OpenWeatherAPI.

### Kereta

Fitur ini dapat memberitahukan jadwal kedatangan kereta selanjutnya di stasiun keberangkatan serta waktu tiba serta biaya yang dibutuhkan memanfaatkan API Google Maps.

## URL

https://gitlab.com/adprog-bonus-c/adprog-bonus/

## Profil Kode

[![coverage report](https://gitlab.com/adprog-bonus-c/adprog-bonus/badges/master/coverage.svg)]((https://gitlab.com/adprog-bonus-c/adprog-bonus/commits/master)

Proses kompilasi, *build*, *testing*, serta *packaging* dari *source code*
chatbot ini dilakukan dengan bantuan *tool* Maven. Berkas [pom.xml](pom.xml)
mengandung daftar lengkap *library* yang dibutuhkan.

Beberapa *library* dependensi utama dari chatbot ini adalah sebagai berikut:

- [TelegramBots](https://github.com/rubenlagus/TelegramBots)
- [JSoup](https://mvnrepository.com/artifact/org.jsoup/jsoup)
- [JUnit 5](https://junit.org/junit5/docs/current/user-guide/)

Chatbot ini dikembangkan tanpa menggunakan *framework*, alias Java polos.
Proses CI/CD juga telah diimplementasikan dengan menggunakan GitLab CI yang
melakukan *automated deployment* ke Heroku.
