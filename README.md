# Grup Bonus Adprog

- Arga Ghulam Ahmad
- Kerenza Doxolodeo
- Made Wira Dhanar Santika
- Geraldo William

## Cara pemakaian

- Maven digunakan untuk handling package secara otomatis
- Setting environment variable di Heroku dan di Gitlab
 + HEROKU_APIKEY , HEROKU_APPNAME , HEROKU_APP_HOST dibutuhkan untuk proses deploy. HEROKU_APIKEY dapat didapatkan dari dashboard Heroku. Tidak dibutuhkan untuk ditambahkan ke environment variable Heroku.
 + GOOGLE_MAPS_API dibutuhkan untuk mengakses jadwal kereta via Google Maps
- File di run dengan cara meng-run src\main\java\Main.java (dalam Heroku, diatur dengan Procfile)


## Detail Bot:

[![coverage report](https://gitlab.com/adprog-bonus-c/adprog-pusilkom-versi-kereta/badges/master/coverage.svg)]((https://gitlab.com/adprog-bonus-c/adprog-pusilkom-versi-kereta/commits/master)
)

### Produksi

Nama bot di Telegram: UncleAdprog

[![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/Uncle_Adprog_Bot)

### Development

Nama bot versi development di Telegram: UncleAdprogDev

[![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/Uncle_AdprogDev)

### Package

Chatbot berbasiskan Java ini memakai *package*
[TelegramBots](https://github.com/rubenlagus/TelegramBots).

## Daftar fitur 

- [x] Announcement - Arga Ghulam Ahmad
- [x] Jadwal - Kerenza Doxoldeo
- [x] Waktu - Dhanar Santika
- [x] Weather - Geraldo William
- [x] Kereta - Dhanar Santika

## Cara kerja Bot 

Bot ini menggunakan design pattern chain of command dengan cara berikut.

Setiap fitur mempunyai 2 package, satu package di directory src\main\java\bot\handler (ketika sebut sebagai Handler) dan satu sub-directory. di directory src\main\java\bot\processor.

Di sub-package yang di dalam Handler, harus ada 1 kelas yang meng-implement Handlerable (kita sebut sebagai kelas Handler)

Di sub-package yang di dalam Processor, harus ada 1 kelas yang meng-implement Processable (kita sebut sebagai kelas Processor)

Ketika sebuah pesan masuk, bot akan meng-iterate daftar Handler di Main.java.

Bot akan memanggil kelas Processor pertama dengan memanggil implementasi kelas tersebut dari `process()`. Jika kelas fitur itu menganggap pesan itu bukan tanggung-jawab fitur dia, kelas itu akan meng-return false.

Jika Bot itu menganggap pesan itu tanggung-jawab dia, pemanggilan `process()` dari kelas itu akan meng-return true. Dengan meng-return true, main loop akan break dan pesan itu lalu di claim oleh kelas fitur itu. Kelas Processor itu akan memasukan pesan ke queue miliki pasagan Handlernya. 

Kelas Handler yang dari awal me-loop membaca dari Queue memproses datanya.

Jika pesan itu tidak ada yang meng-klaim, maka bot akan mengirim pesan kepada pengguna yang mengatakan bahwa pesan tersebut invalid.

## Cara kerja menambahkan fitur

Setiap fitur mempunyai 2 package, satu package di directory src\main\java\bot\handler (ketika sebut sebagai Handler) dan satu sub-directory. di directory src\main\java\bot\processor.

Di sub-package yang di dalam Handler, harus ada 1 kelas yang meng-implement Handlerable.

Di sub-package yang di dalam Processor, harus ada 1 kelas yang meng-implement Processable.

NOTE : Di dokumentasi TelegramBot, anda bisa mengirim pesan menggunakan execute(). Gunakan send() daripada execute(). Send adalah wrapper execute() yang dioptimisasi untuk proses Testing.

Ubah constructor AmazingBot.java agar kelas fitur terdaftar di ArrayList.

## Cara kerja Testing

Contoh kerja testing dapat dilihat di src\test\java\bot\Time\SceleTimeTest.java, tetapi pada dasarnya ada beberapa hal yang perlu diperhitungkan.

1. AmazingBot (dan bot yang anda buat yang meng-extend AbstractBot) untuk keperluan testing harus di inti dengan mode = True. Ketika anda meng init mode = True, object yang anda coba send() akan masuk ke arrayList testingLog untuk anda inspeksi, sementara jika menggunakan mode = False, object yang anda coba send() akan dikirim langsung.

2. Objek Update berguna untuk mengsimulasi transaksi pesan dari server Telegram. Anda dapat mengkonstruksi Update dengan mengkonversi JSON -> Update menggunakan ObjectMapper

## Reminder

- Kode kamu harus mencegah racing condition. Selalu asumsikan kode kamu akan dipakai dalam konteks multi-threading
* TelegramBots bersifat sinkronus jadi kalau fitur kamu lama, bakal mandek. Salah satu alasan program kamu bisa mandek adalah melakukan scrapping atau API CS UI. (Satu pemanggilan API CS UI kurang lebih dua detik) Cache ketika kamu bisa, terutama data yang jarang berubah.

## Resources

- Dokumentasi Library Bot Telegram : https://github.com/rubenlagus/TelegramBots
- Tutorial Bot Telegram menggunakan Library TelegramBots : https://legacy.gitbook.com/book/monsterdeveloper/writing-telegram-bots-on-java/details (Bab 1, Bab 5, Bab 6 menarik)
