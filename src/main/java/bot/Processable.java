package bot;


import org.telegram.telegrambots.api.objects.Update;

public interface Processable {
    public boolean process(Update message);
}
