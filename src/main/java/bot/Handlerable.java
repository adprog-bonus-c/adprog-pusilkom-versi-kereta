package bot;

import org.telegram.telegrambots.api.objects.Update;

public interface Handlerable {

    public void processMessage(Update update);
}
