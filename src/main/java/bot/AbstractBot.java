package bot;

import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractBot extends TelegramLongPollingBot {
    private boolean testingMode = false;
    public ArrayList<String> testingLog = new ArrayList<String>();

    public AbstractBot(boolean mode){
        this.testingMode = mode;
    }


    /**
     *
     * <p>NOTE: Alasan mengapa metode execute harus di-wrap oleh metode ini berguna untuk mendukung proses Testing</p>
     * </p>
     * @param method
     * @param <T>
     * @param <Method>
     * @return
     * @throws TelegramApiException
     */
    public <T extends Serializable, Method extends BotApiMethod<T>> boolean send(Method method){
        if(testingMode){
            testingLog.add(method.toString());
            return true;
        }
        try {
            execute(method);
            return true;
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return false;
        }
    }
}
