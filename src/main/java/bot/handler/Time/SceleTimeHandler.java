package bot.handler.Time;

import bot.AbstractBot;
import bot.Handlerable;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.util.concurrent.LinkedBlockingQueue;

public class SceleTimeHandler implements Runnable, Handlerable {
    AbstractBot bot;
    LinkedBlockingQueue<Update> queue;


    private final boolean FORMAT_IN_DATE = true;

    public SceleTimeHandler(AbstractBot bot){
        this.bot = bot;
        this.queue = new LinkedBlockingQueue<Update>();
    }

    public synchronized void processMessage(Update update) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        String replyText = null;
        if (text.equalsIgnoreCase("/help waktu")) {
            message.setText("Format: /waktu\n" +
                    "Mengembalikan waktu server scele\n" +
                    "Contoh: /waktu akan mengambilkan tanggal dan waktu saat ini");
            this.bot.send(message);
            return;
        } else if(text.equalsIgnoreCase("/waktu")) {
            replyText = TimeFetcher.getTime(TimeFetcher.getTimeServer(), FORMAT_IN_DATE);
        } else if(text.startsWith("/waktu")){
            replyText = "Argumen /waktu tidak butuh argumen";
        }
        message.setText(replyText);
        this.bot.send(message);
    }

    public void run(){
        while(true) {
            Update update = this.queue.poll();
            if(update != null) {
                processMessage(update);
            }
        }
    }

    public LinkedBlockingQueue<Update> getQueue(){
        return this.queue;
    }
}
