package bot.handler.Time;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeFetcher {

    private static final String TIME_SERVER = "ntp.ui.ac.id";
    private static final String TIME_FORMAT = "EEE MMM dd HH:mm:ss z yyyy";

    public static synchronized String getTimeServer() { return TIME_SERVER; }

    public static synchronized String getTime(String host, boolean FORMAT_IN_DATE) {
        NTPUDPClient client = new NTPUDPClient();
        String text;
        try {
            InetAddress inetAddress = InetAddress.getByName(host);
            TimeInfo timeInfo = client.getTime(inetAddress);
            long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
            Date date = new Date(returnTime);
            if(FORMAT_IN_DATE)
                text = "Waktu sekarang: " + convertToWIB(date);
            else {
                long secs = (date.getTime() / 1000) + 60;
                text = String.valueOf(secs);
            }
        } catch (IOException e) {
            text = "Unknown Hosts";
        }
        return text;
    }

    public static synchronized String convertToWIB(Date date) {
        DateFormat formatter = new SimpleDateFormat(TIME_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        return formatter.format(date);
    }
}
