package bot.handler.Train;

import bot.*;
import bot.handler.Time.TimeFetcher;
import org.json.JSONObject;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import static java.lang.Math.abs;

public class TrainScheduleHandler implements Runnable, Handlerable {
    AbstractBot bot;
    LinkedBlockingQueue<Update> queue;
    Jedis jedis;
    protected String API_URL = "https://maps.googleapis.com/maps/api/directions/json?";
    private final boolean FORMAT_IN_SECS = false;


    public TrainScheduleHandler(AbstractBot bot){
        this.bot = bot;
        this.queue = new LinkedBlockingQueue<Update>();
        this.jedis = JedisFactory.getInstance().getJedisPool().getResource();
        this.jedis.auth("redispusilkom");
    }

    public synchronized void processMessage(Update update) {
        String sender = Util.getUserId(update);
        boolean flag = false;

        /*
        Ada 4 kemungkinan untuk status

         - null : artinya user pertama kali pakai fitur ini
         - "--------" : user tidak direserve oleh fitur apapun
         - "KR" : artinya user sedang di-reserve untuk dihandle oleh fitur ini
         - "KRXX" : XX adalah angka yang menunjukan urutan stasiun asal di TrainStation.values()
         contoh : 00 menujuk pada TrainStation.values[0] yaitu Tangerang
         */
        String status = jedis.get(sender);

        if(update.hasMessage() && update.getMessage().hasText()) {
            long chatId = update.getMessage().getChatId();
            SendMessage message = new SendMessage().setChatId(chatId);
            String text = update.getMessage().getText().trim().toLowerCase();
            String tokenList[] = text.split(" ");
            /*
            Cari kata "kereta" atau "jadwal" di pesan.
            Jika muncul, artinya user ingin mencari jadwal kereta
             */
            if (status == null || status.equals("--------")) {
                status = "--------";
                int smallestEdit = 2;
                for (String token : tokenList) {
                    // Cari kata kereta
                    if (abs(token.length() - 6) < 2) {
                        smallestEdit = Math.min(Util.editDistance(token, "kereta"), smallestEdit);
                    }
                    // Cari kata jadwal
                    if (abs(token.length() - 6) < 2) {
                        smallestEdit = Math.min(Util.editDistance(token, "jadwal"), smallestEdit);
                    }
                    if (smallestEdit == 0) {
                        break;
                    }
                }
                // Jika mereka ingin mencari jadwal, reserve user itu untuk fitur kita
                if (smallestEdit == 0) {
                    status = "KR";
                    flag = true;
                }
                // Kalau edit distancenya == 1 , konfirmasi apakah mereka ingin cek kereta?
                if (smallestEdit == 1) {
                    InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    List<InlineKeyboardButton> rowInline = new ArrayList<>();
                    message.setText("Apakah anda ingin memeriksa jadwal kereta?");
                    rowInline.add(new InlineKeyboardButton().setText("Akses Jadwal kereta").setCallbackData("kereta"));
                    rowsInline.add(rowInline);
                    markupInline.setKeyboard(rowsInline);
                    message.setReplyMarkup(markupInline);
                    this.bot.send(message);
                }
            }
            if (status.startsWith("KR")) {
                TrainStation[] listOfTrainStation = TrainStation.values();

                ArrayList<TrainStation> stationWithSmallestEdit = new ArrayList<>();
                int smallestEdit = 1;
                for (int I = 0; I < tokenList.length; I++) {
                    for (TrainStation station : TrainStation.values()) {
                        int singkatanEditDistance = Math.min(3, Util.editDistance(tokenList[I], station.getSingkatan().toLowerCase()));
                        String[] namaStasiunTokenList = station.getNamaStasiun().split(" ");
                        if (I + namaStasiunTokenList.length - 1 < tokenList.length) {
                            int stasiunEditDistance = 0;
                            for (int J = 0; J < namaStasiunTokenList.length; J++) {
                                if(Math.abs(tokenList[I + J].length() - namaStasiunTokenList[J].length()) < 2) {
                                    stasiunEditDistance += Util.editDistance(tokenList[I + J], namaStasiunTokenList[J].toLowerCase());
                                }
                                else{
                                    stasiunEditDistance += 2;
                                }
                            }
                            if (smallestEdit > Math.min(singkatanEditDistance, stasiunEditDistance)) {
                                smallestEdit = Math.min(singkatanEditDistance, stasiunEditDistance);
                                stationWithSmallestEdit.clear();
                            }
                            if (smallestEdit >= Math.min(singkatanEditDistance, stasiunEditDistance)) {
                                stationWithSmallestEdit.add(station);
                            }
                        } else {
                            if (smallestEdit > singkatanEditDistance) {
                                smallestEdit = singkatanEditDistance;
                                stationWithSmallestEdit.clear();
                            }
                            if (smallestEdit >= singkatanEditDistance) {
                                stationWithSmallestEdit.add(station);
                            }
                        }
                    }
                }
                // Artinya ketemu satu stasiun. Catat
                if (smallestEdit == 0 && stationWithSmallestEdit.size() == 1) {
                    int index = stationWithSmallestEdit.get(0).ordinal();
                    // Berati stasiun kita temukan itu stasiun asal
                    if (status.equals("KR")) {
                        message.setText("Bot mendeteksi anda berangkat dari stasiun " +
                                stationWithSmallestEdit.get(0).getNamaStasiun()
                                + "Masukan stasiun tujuan anda.");
                        this.bot.send(message);
                        // Catat stasiun awalnya
                        if(index < 10){
                            status = "KR0" + index;
                        }
                        else{
                            status = "KR" + index;
                        }
                    }
                    // Berati stasuiun kita itu stasiun tujuan
                    else {
                        int indexKeretaKeberangkatan = Integer.parseInt(status.substring(2, 4));
                        status = "--------";
                        String dateInSecs = TimeFetcher.getTime(TimeFetcher.getTimeServer(), FORMAT_IN_SECS);
                        if(listOfTrainStation[indexKeretaKeberangkatan].getNamaStasiun().equals(listOfTrainStation[index].getNamaStasiun())){
                            message.setText("Anda tidak bisa pergi dan datang dari stasiun yang sama ");
                            bot.send(message);
                        }
                        else {
                             getSchedule(dateInSecs,
                                     "stasiun " + TrainStation.values()[indexKeretaKeberangkatan].getNamaStasiun(),
                                    "stasiun " + TrainStation.values()[index].getNamaStasiun(), message);
                             bot.send(message);
                        }
                    }
                }
                // Kita tidak menemukan stasiun
                else if (stationWithSmallestEdit.size() == 0) {
                    if (status.equals("KR") && flag) {
                        message.setText("Masukan stasiun keberangkatan kereta anda.");
                        this.bot.send(message);
                    } else {
                        message.setText("Bot tidak mengerti stasiun kereta anda");
                        this.bot.send(message);
                    }
                }
                // Artinya ada beberapa stasiun
                else {
                    message.setText("Bot mendeteksi beberapa stasiun yang mungkin anda maksud");
                    InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
                    List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
                    for (TrainStation possibleStation : stationWithSmallestEdit) {
                        List<InlineKeyboardButton> rowInline = new ArrayList<>();
                        rowInline.add(new InlineKeyboardButton()
                                .setText(possibleStation.getNamaStasiun())
                                .setCallbackData(possibleStation.getSingkatan()));
                        rowsInline.add(rowInline);
                    }
                    markupInline.setKeyboard(rowsInline);
                    message.setReplyMarkup(markupInline);
                    status = status;
                    this.bot.send(message);
                }
            }
        }

        // User menekan tombol
        else if(update.hasCallbackQuery()){
            long chatId = update.getCallbackQuery().getMessage().getChatId();
            SendMessage message = new SendMessage().setChatId(chatId);

            String buttonText = update.getCallbackQuery().getData();
            System.out.println(buttonText);
            // Jika statusnya benar, cari stasiun yang cocok dengan input tombol
            if(buttonText.startsWith("CKS")){
                String cks[] = buttonText.split("#");
                getSchedule(cks[3],"stasiun "+cks[1],"stasiun "+cks[2], message);
                status = "--------";
                bot.send(message);
            }
            if (status.contains("KR")) {
                for (TrainStation stasiun : TrainStation.values()) {
                    if (buttonText.equals(stasiun.getSingkatan())) {
                        int value = stasiun.ordinal();
                        if (status.equals("KR")) {
                            if (value < 10) {
                                status = "KR0" + value;
                            } else {
                                status = "KR" + value;
                            }
                            message.setText("Anda berangkat dari " + stasiun.getNamaStasiun() +
                                    ". Anda akan pergi ke stasiun mana? ");
                        } else {
                            int indeks = Integer.parseInt(status.substring(2, 4));
                            status = "--------";
                            String dateInSecs = TimeFetcher.getTime(TimeFetcher.getTimeServer(), FORMAT_IN_SECS);
                            getSchedule(dateInSecs,"stasiun "+TrainStation.values()[indeks].getNamaStasiun(),
                                    "stasiun "+TrainStation.values()[value].getNamaStasiun(),message);
                        }
                        bot.send(message);
                    }
                }
            }
            else if(buttonText.equals("kereta")){
                status = "KR";
                message.setText("Masukan stasiun keberangkatan anda.");
                bot.send(message);
            }
        }
        jedis.set(sender,status);
    }

    public void run(){
        while(true) {
            Update update = this.queue.poll();
            if(update != null) {
                System.out.println("START>");
                processMessage(update);
                System.out.println("FINISH<");
            }
        }
    }

    public LinkedBlockingQueue<Update> getQueue(){
        return this.queue;
    }

    public synchronized void generateScheduleMessage(String replyText, int epoch, SendMessage message){
        if(replyText.startsWith("Keberangkatan dari")) {
            String stasiunAsal = replyText.split(" pada")[0].split("stasiun ")[1];
            String stasiunTujuan = replyText.split(" pada")[1].split("stasiun ")[1];

            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton()
                    .setText("Cek kereta selanjutnya")
                    .setCallbackData("CKS #"
                            + stasiunAsal
                            + "#"
                            + stasiunTujuan
                            + "#"
                            + (epoch+61)));
            rowsInline.add(rowInline);
            markupInline.setKeyboard(rowsInline);
            message.setReplyMarkup(markupInline);
        }
    }

    public synchronized void getSchedule(String seconds, String stasiunAwal, String stasiunTujuan, SendMessage message) {
        String awal = stasiunAwal.replace(" ","+");
        String tujuan = stasiunTujuan.replace(" ","+");
        String url = API_URL + "origin=" + awal + "&destination=" + tujuan + "&mode=transit&transit_mode=train" +
                "&departure_time=" + seconds + "&key=" + Authorization.getGoogleMapsAPI();
        int epochKedatangan;
        String text = "Stasiun tidak ditemukan";
        try {
            String data = readJSONFromURL(url).toString();
            JSONObject json = new JSONObject(data.trim());
            String status = json.get("status").toString();
            if(status.equalsIgnoreCase("OK")) {
                String routesString = json.getJSONArray("routes").toString();
                routesString = routesString.substring(1,routesString.length()-1);
                JSONObject routes = new JSONObject(routesString);
                JSONObject fare = routes.getJSONObject("fare");
                String legsString = routes.getJSONArray("legs").toString();
                legsString = legsString.substring(1,legsString.length()-1);
                JSONObject legs = new JSONObject(legsString);
                JSONObject arrival = legs.getJSONObject("arrival_time");
                JSONObject departure = legs.getJSONObject("departure_time");
                String harga = fare.get("text").toString();
                String keberangkatan = departure.get("text").toString();
                String kedatangan = arrival.get("text").toString();
                epochKedatangan = Integer.parseInt(departure.get("value").toString());
                text = "Keberangkatan dari " + stasiunAwal + " pada pukul " + keberangkatan +
                        " tiba di " + stasiunTujuan + " pada pukul " + kedatangan + " dengan tarif " + harga;
                generateScheduleMessage(text,epochKedatangan,message);
            }
            message.setText(text);
        } catch (IOException e){
            e.printStackTrace();
            message.setText("Invalid URL");
        }
    }

    public synchronized JSONObject readJSONFromURL(String url) throws IOException {
        InputStream data = new URL(url).openStream();
        JSONObject json;
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(data, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            json = new JSONObject(jsonText);
        } finally {
            data.close();
        }
        return json;
    }

    private synchronized String readAll(BufferedReader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }


}
