package bot;

import org.jetbrains.annotations.Nullable;
import org.json.simple.parser.JSONParser;
import org.telegram.telegrambots.api.objects.Update;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Util {

    @Nullable
    public static synchronized Object makeCall(String url) {
        try {
            URL obj = new URL(url);


            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setDoInput(true);
            con.setRequestMethod("GET");

            StringBuffer response = new StringBuffer();

            // Kredensial invalid tidak akan mendapat respons 200
            if (con.getResponseCode() == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONParser parser = new JSONParser();
                if(response.toString().charAt(0)=='<')
                    return null;
                Object jsonObject = parser.parse(response.toString());

                return jsonObject;
            } else {
                System.out.println(con.getResponseCode());
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static synchronized int min(int x,int y,int z) {
        // Mengembalikan nilai terkecil
        if (x <= y && x <= z) return x;
        if (y <= x && y <= z) return y;
        else return z;
    }

    public static synchronized String getUserId(Update update){
        if(update.hasMessage()){
            return update.getMessage().getFrom().getId().toString();
        }
        else if(update.hasCallbackQuery()){
            return update.getCallbackQuery().getFrom().getId().toString();
        }
        return null;
    }

    public static synchronized int editDistance(String str1, String str2) {
        int m = str1.length();
        int n = str2.length();
        // Buat tabel Dynamic Programming
        int dp[][] = new int[m+1][n+1];

        // Isi secara bottom up
        for (int i=0; i<=m; i++)
        {
            for (int j=0; j<=n; j++)
            {
                // Kalau string pertama kosong, banyak operasi
                // sebanyak string kedua
                if (i==0)
                    dp[i][j] = j;  // Min. operations = j

                // Kalau string kedua kosong, banyak operasi
                // sebanyak string pertama
                else if (j==0)
                    dp[i][j] = i; // Min. operations = i

                // Kalau char awal dan akhir kedua string sama,
                // cukup perhatikan tengahnya saja
                else if (str1.charAt(i-1) == str2.charAt(j-1))
                    dp[i][j] = dp[i-1][j-1];

                // Kalau char awal dan akhir kedua string beda,
                // coba dilakukan tiga operasi pada kedua string
                else
                    dp[i][j] = 1 + min(dp[i][j-1],  // Insert
                            dp[i-1][j],  // Remove
                            dp[i-1][j-1]); // Replace
            }
        }

        return dp[m][n];
    }

}

