package bot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.net.ssl.HttpsURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Authorization {
    /**
     * Authorization adalah kelas yang menampung segala hal yang dibutuhkan untuk authorization bot ke
     * API CS UI.
     */

    /* Hehe. Saya percaya anda tidak bakal jebol akun saya. Nanti suatu saat akan saya
       buat versi pakai system environment. <3
       UPDATE: DONE!!
     */
    private static String username = System.getenv("SSO_USERNAME");
    private static String password = System.getenv("SSO_PASSWORD");
    private static String clientId = System.getenv("CLIENT_ID");
    private static String usernameTelegram = System.getenv("USERNAME_TELEGRAM");
    private static String accessTokenTelegram = System.getenv("ACCESS_TOKEN_TELEGRAM");
    private static String googleMapsAPI = System.getenv("GOOGLE_MAPS_API");
    private static String accessCode;
    private static Long lastFetched;
    // PANJANG!
    private static String authorization = "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW4"
            +"3RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxu"
            +"VHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym"
            +"1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==";

    /**
     * Mengembalikan client code untuk kepentingan akses API CS UI.
     *
     * @return      sebuah string berupa access code tersebut. Perhatikan bahwa
     *              mungkin buat fungsi tersebut untuk meng-return null atau access code yang sudah expired
     *              jika terjadi gangguan teknis. Bisa dicoba ulang dengan cara memanggil fungsi ini kembali
     */
    public static String getClientId(){
        return clientId;
    }

    /**
     * Mengembalikan access code untuk kepentingan akses API CS UI. Jika access code expired atau belum di-generate,
     * maka fungsi tersebut akan mencoba meng-generate access code tersebut.
     * <p>
     * Note bahwa access code ini hanya bisa dipakai untuk mengakses fungsi API CS UI yang tidak
     * mengakses data private. Untuk yang butuh data private (seperti data akademis) dia harus pakai fungsi
     * validateAndGetAccessCode()
     *
     * @return      sebuah string berupa access code tersebut. Perhatikan bahwa
     *              mungkin buat fungsi tersebut untuk meng-return null atau access code yang sudah expired
     *              jika terjadi gangguan teknis. Bisa dicoba ulang dengan cara memanggil fungsi ini kembali.
     */
    public static synchronized String getAccessCode()  {

        //Access Code memiliki expired Time 36000 detik. Karena fetch access code itu mahal
        //(2 sampai 3 detik) jadi di ambil dari cache saja jika masih ada selisih waktu
        //yang lama
        if(lastFetched != null){
            Long currentEpoch = System.currentTimeMillis() / 1000L;
            if(currentEpoch-lastFetched < 30000){
                return accessCode;
            }
        }
        String result = validateAndGetAccessCode(username,password);

        if (result != null && result != accessCode){
            lastFetched = System.currentTimeMillis() / 1000L;
            accessCode = result;
        }

        return accessCode;
    }

    /**
     * <p>Mengvalidasi kredensial dan mengembalikan access code untuk kepentingan akses API CS UI.</p>
     * <p>
     * Perhatikan bahwa fungsi ini dikhususkan untuk mengakses fungsi API yang meng-return data yang private.
     * Kode access code ini hanya bisa dipakai untuk kredensial yang divalidasi. Untuk kode access yang tidak private
     * (daftar kelas) pakai getAccessCode() untuk optimisasi run Time</p>
     * <p>* Ingat bahwa pemanggilan validateAndGetAccessCode mahal (2 ~ 3 detik)
     * sehingga idealnya dihemat (ie. cache.)</p>
     * <p> NOTE: BELUM BISA DIPAKAI JADI DIBUAT PRIVATE DULU</p>
     *
     * @return      sebuah string berupa access code untuk user tersebut jika kredensial valid
     *              sebuah string kosong jika kredensial invalid
     *              atau null jika masalah teknis
     */
    public static synchronized String validateAndGetAccessCode(String username, String password){

        StringBuilder tokenUri=new StringBuilder("username=");
        try {
            tokenUri.append(URLEncoder.encode(username, "UTF-8"));
            tokenUri.append("&password=");
            tokenUri.append(URLEncoder.encode(password, "UTF-8"));
            tokenUri.append("&grant_type=");
            tokenUri.append(URLEncoder.encode("password", "UTF-8"));

            String url = "https://akun.cs.ui.ac.id/oauth/token/";
            URL obj = new URL(url);


            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            String accessCode = null;
            con.setDoOutput(true);
            con.setDoInput(true);

            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", Authorization.authorization);

            OutputStream os = con.getOutputStream();
            os.write(tokenUri.toString().getBytes());
            os.close();
            // Kredensial invalid tidak akan mendapat respons 200
            if (con.getResponseCode() == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                accessCode = response.toString().substring(18, 48);
            }
            return accessCode;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Mengembalikan username dari bot telegram
     * @return  String yang berisi username
     */
    public static synchronized String getUsernameTelegram() { return usernameTelegram; }

    /**
     * Mengembalikan access token dari bot telegram
     * @return  String yang berisi access token
     */
    public static synchronized String getAccessTokenTelegram() { return accessTokenTelegram; }

    /**
     * Mengembalikan key dari google maps API
     * @return String yang berisi key
     */
    public static synchronized String getGoogleMapsAPI() { return googleMapsAPI; }

}
