package bot;


import bot.handler.Time.SceleTimeHandler;
import bot.handler.Train.TrainScheduleHandler;
import bot.processor.Time.SceleTimeProcessor;

import bot.processor.Train.TrainScheduleProcessor;
import org.telegram.telegrambots.api.objects.Update;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class AmazingBot extends AbstractBot {

    ArrayList<Runnable> listOfHandler = new ArrayList<Runnable>();
    ArrayList<Processable> listOfProcess = new ArrayList<Processable>();
    HashMap<String,LinkedBlockingQueue<Update>> hashMapOfQueue = new HashMap<String,LinkedBlockingQueue<Update>>();

    public AmazingBot(boolean mode){
        super(mode);
        listOfProcess.add(new SceleTimeProcessor(hashMapOfQueue,this));
        listOfProcess.add(new TrainScheduleProcessor(hashMapOfQueue, this));

        TrainScheduleHandler train = new TrainScheduleHandler(this);
        hashMapOfQueue.put("kereta", train.getQueue());
        if(!mode) {
            Thread thread1 = new Thread(train);
            thread1.start();
        }

        SceleTimeHandler sceleClock = new SceleTimeHandler(this);
        hashMapOfQueue.put("jam scele", sceleClock.getQueue());
        if(!mode) {
            Thread thread2 = new Thread(sceleClock);
            thread2.start();
        }

    }


    public void onUpdateReceived(Update update) {

        // Pertama-tama, kita mencari fitur mana yang mengklaim bahwa pesan ini
        // dihandle oleh implementasi dia (ie. Design Pattern Chain of Command)
        System.out.println(update);
        for(Processable process : listOfProcess){
            if(process.process(update)){
                return;
            }
        }
    }

    /**
     * Jika usernamenya @MyAmazingBot, hasil kembalian juga harus "MyAmazingBot"
     * @return  String berupa username bot
     */

    public String getBotUsername() { return Authorization.getAccessTokenTelegram(); }

    /**
     * Mengembalikan token untuk BotFather
     * @return  String berupa access token bot
     */
    @Override
    public String getBotToken() { return Authorization.getAccessTokenTelegram(); }

    public HashMap<String,LinkedBlockingQueue<Update>> getHashMapOfQueue(){
        return this.hashMapOfQueue;
    }

}