package bot.processor.Train;

import bot.AbstractBot;
import bot.JedisFactory;
import bot.Processable;
import bot.Util;
import org.telegram.telegrambots.api.objects.Update;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class TrainScheduleProcessor implements Processable {
    HashMap<String, LinkedBlockingQueue<Update>> queues;
    Jedis jedis;

    public TrainScheduleProcessor(HashMap<String, LinkedBlockingQueue<Update>> listOfQueue, AbstractBot object) {
        this.jedis = JedisFactory.getInstance().getJedisPool().getResource();
        this.jedis.auth("redispusilkom");
        this.queues = listOfQueue;
    }

    public synchronized boolean process(Update update) {
        String sender = Util.getUserId(update);
        String status = jedis.get(sender);
        if (status == null || status.equals("--------")){
            if(update.hasMessage() && update.getMessage().hasText()) {
                String text = update.getMessage().getText().toLowerCase();
                String[] tokenList = text.split(" ");
                boolean result = false;
                for (String token : tokenList) {
                    // Cari kata kereta
                    if (abs(token.length() - 6) < 2) {
                        result |= Util.editDistance(token, "kereta") < 2;
                    }
                    // Cari kata jadwal
                    if (abs(token.length() - 5) < 2) {
                        result |= Util.editDistance(token, "jadwal") < 2;
                    }
                }
                if (result) {
                    try {
                        this.queues.get("kereta").put(update);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            }
            else if(update.hasCallbackQuery()){
                String buttonText = update.getCallbackQuery().getData();
                if(buttonText.equals("kereta") || buttonText.contains("CKS")){
                    try {
                        this.queues.get("kereta").put(update);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                }

            }
        }
        else if (status.startsWith("KR")) {
            try {
                this.queues.get("kereta").put(update);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }
}
