package bot.processor.Time;

import bot.AbstractBot;
import bot.Processable;
import org.telegram.telegrambots.api.objects.Update;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class SceleTimeProcessor implements Processable {
    HashMap<String,LinkedBlockingQueue<Update>> queues;
    public SceleTimeProcessor(HashMap<String,LinkedBlockingQueue<Update>> listOfQueue, AbstractBot object ){
        this.queues = listOfQueue;
    }

    public synchronized boolean process(Update update){
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase("/help waktu") || (text.toLowerCase()).startsWith("/waktu")) {
                boolean attemptFinished = false;
                while(!attemptFinished) {
                    try {
                        this.queues.get("jam scele").put(update);
                        attemptFinished = true;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        }
        return false;
    }
}
