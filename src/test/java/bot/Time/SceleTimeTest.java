package bot.Time;

import bot.AmazingBot;
import bot.handler.Time.SceleTimeHandler;
import bot.processor.Time.SceleTimeProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SceleTimeTest {

    static AmazingBot amazingBot;
    static SceleTimeProcessor timeProcessor;
    static SceleTimeHandler timeHandler;
    static HashMap<String,LinkedBlockingQueue<Update>> hashMapOfQueue;

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Terjadi kesalahan pada konversi JSON -> Update");
            return null;
        }
    }

    @BeforeClass
    public static void setup(){
        SceleTimeTest.amazingBot = new AmazingBot(true);
        SceleTimeTest.hashMapOfQueue = SceleTimeTest.amazingBot.getHashMapOfQueue();
        SceleTimeTest.timeProcessor = new SceleTimeProcessor(
                SceleTimeTest.hashMapOfQueue, SceleTimeTest.amazingBot);
        SceleTimeTest.timeHandler = new SceleTimeHandler(SceleTimeTest.amazingBot);
    }

    @After
    public void testingLogCleanUp(){
        SceleTimeTest.amazingBot.testingLog.clear();
    }

    @Test
    public void testProcessor_VanillaWaktu(){
        String entry = "{\"message\":{\"text\":\"/waktu\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = SceleTimeTest.timeProcessor.process(upd);
        assertTrue(result);
    }

    @Test
    public void testProcessor_InvalidArgument(){
        String entry = "{\"message\":{\"text\":\"/waktu hehe\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = SceleTimeTest.timeProcessor.process(upd);
        assertTrue(result);
    }

    @Test
    public void testProcessor_Help(){
        String entry = "{\"message\":{\"text\":\"/help waktu\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = SceleTimeTest.timeProcessor.process(upd);
        assertTrue(result);
    }

    @Test
    public void testProcessor_InvalidQuery(){
        String entry = "{\"message\":{\"text\":\"waktu\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = SceleTimeTest.timeProcessor.process(upd);
        assertTrue(!result);
    }

    @Test
    public void testHandler_VanillaKereta(){
        String entry = "{\"message\":{\"text\":\"/waktu\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        SceleTimeTest.timeHandler.processMessage(upd);
        System.out.println(SceleTimeTest.amazingBot.testingLog.size());
        assertTrue(SceleTimeTest.amazingBot.testingLog.size()==1);
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("Waktu sekarang"));
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("WIB"));
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("chatId='10'"));
    }

    @Test
    public void testHandler_Help(){
        String entry = "{\"message\":{\"text\":\"/help waktu\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        SceleTimeTest.timeHandler.processMessage(upd);
        System.out.println(SceleTimeTest.amazingBot.testingLog.size());
        assertTrue(SceleTimeTest.amazingBot.testingLog.size()==1);
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("Format: /waktu\n"));
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("chatId='10'"));
    }


    @Test
    public void testHandler_InvalidArgument(){
        String entry = "{\"message\":{\"text\":\"/waktu hehe\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        SceleTimeTest.timeHandler.processMessage(upd);
        System.out.println(SceleTimeTest.amazingBot.testingLog.size());
        assertTrue(SceleTimeTest.amazingBot.testingLog.size()==1);
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("text='Argumen /waktu tidak butuh argumen"));
        assertTrue(SceleTimeTest.amazingBot.testingLog.get(0).contains("chatId='10'"));
    }
}
