package bot.Time;

import bot.AmazingBot;
import bot.handler.Time.SceleTimeHandler;
import bot.handler.Time.TimeFetcher;
import bot.processor.Time.SceleTimeProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TimeFetcherTest {
    static AmazingBot amazingBot;
    static SceleTimeProcessor timeProcessor;
    static SceleTimeHandler timeHandler;
    static HashMap<String,LinkedBlockingQueue<Update>> hashMapOfQueue;

    @Test
    public void testGetTimeNoHost() {
        String tester = TimeFetcher.getTime("123",true);
        assertEquals("Unknown Hosts",tester);
    }
}
