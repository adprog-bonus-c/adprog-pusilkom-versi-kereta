package bot;

import bot.Time.SceleTimeTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static bot.Util.editDistance;
import static org.junit.Assert.*;

public class UtilTest {

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Terjadi kesalahan pada konversi JSON -> Update");
            return null;
        }
    }

    @Test
    public void editDistanceTest() {
        String str1 = "depok";
        String str2 = "sepak";
        int tester = editDistance(str1, str2);
        assertEquals(2, tester);
    }


    @Test
    public void thereIsNeitherMessageNorCallbackTest() {
        String entry = "{\"Test\":\"This is a test\"}";
        Update upd = getUpdateGivenJson(entry);
        String tester = Util.getUserId(upd);
        assertNull(tester);
    }

    @Test
    public void jsonTest() {
        String test = "https://api.4stats.io/board/g";
        String testFail = "https://4stats.io";
        String testFail2 = "https://4stats.i";
        String testFail3 = "https://www.google.com/teapot";
        try {
            System.out.println("Test1");
            assertFalse(Util.makeCall(test) == null);
            System.out.println("Test2");
            assertTrue(Util.makeCall(testFail) == null);
            System.out.println("Test3");
            //assertTrue(Util.makeCall(testFail2) == null);
            System.out.println("Test4");
            assertTrue(Util.makeCall(testFail3) == null);
        } catch (Exception e) {
            fail("Terjadi kesalahan pada pengambilan resource");
        }

    }
}
