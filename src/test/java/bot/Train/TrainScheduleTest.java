package bot.Train;

import bot.AmazingBot;
import bot.JedisFactory;
import bot.Util;
import bot.handler.Train.TrainScheduleHandler;
import bot.processor.Train.TrainScheduleProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TrainScheduleTest {

    static AmazingBot amazingBot;
    static TrainScheduleProcessor trainProcessor;
    static TrainScheduleHandler trainHandler;
    static HashMap<String,LinkedBlockingQueue<Update>> hashMapOfQueue;
    static Jedis jedis;

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Terjadi kesalahan pada konversi JSON -> Update");
            return null;
        }
    }

    @BeforeClass
    public static void setup(){
        TrainScheduleTest.jedis = JedisFactory.getInstance().getJedisPool().getResource();
        TrainScheduleTest.jedis.auth("redispusilkom");
        TrainScheduleTest.amazingBot = new AmazingBot(true);
        TrainScheduleTest.hashMapOfQueue = TrainScheduleTest.amazingBot.getHashMapOfQueue();
        TrainScheduleTest.trainProcessor = new TrainScheduleProcessor(
                TrainScheduleTest.hashMapOfQueue,TrainScheduleTest.amazingBot);
        TrainScheduleTest.trainHandler = new TrainScheduleHandler(TrainScheduleTest.amazingBot);
    }

    @After
    public void testingLogCleanUp(){
        TrainScheduleTest.amazingBot.testingLog.clear();
        TrainScheduleTest.jedis.set("3278","--------");
    }


    @Test
    public void testHandler_VanillaKereta(){

        String entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"/kereta\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
    }

    @Test
    public void testHandler_Kereta() {
        String entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kereta depok\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kereta bogor\",\"chat\":{\"id\":\"10\"}}}";
        upd = getUpdateGivenJson(entry);
        TrainScheduleTest.trainHandler.processMessage(upd);
    }

    @Test
    public void testHandler_KeretaTypo_1() {
        String entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kerera depok\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kereta depok\",\"chat\":{\"id\":\"10\"}}}";
        upd = getUpdateGivenJson(entry);
        result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kereta bogor\",\"chat\":{\"id\":\"10\"}}}";
        upd = getUpdateGivenJson(entry);
        result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
    }

    @Test
    public void testHandler_KeretaWithNoInput() {
        String entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"kereta\",\"chat\":{\"id\":\"10\"}}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"callback_query\":{\"message\":{\"chat\":{\"id\":\"123\"}},\"from\":{\"id\":\"3278\"},\"data\":\"DP\"}}";
        upd = getUpdateGivenJson(entry);
        result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
    }

    @Test
    public void testHandler_KeretaWithButton() {
        String entry = "{\"callback_query\":{\"message\":{\"chat\":{\"id\":\"123\"}},\"from\":{\"id\":\"3278\"},\"data\":\"kereta\"}}";
        Update upd = getUpdateGivenJson(entry);
        boolean result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"message\":{\"from\":{\"id\":3278, \"lastName\":\"test\"," +
                "\"languageCode\":\"en-US\"},\"text\":\"depor\",\"chat\":{\"id\":\"10\"}}}";
        upd = getUpdateGivenJson(entry);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"callback_query\":{\"message\":{\"chat\":{\"id\":\"123\"}},\"from\":{\"id\":\"3278\"},\"data\":\"DP\"}}";
        upd = getUpdateGivenJson(entry);
        result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
        entry = "{\"callback_query\":{\"message\":{\"chat\":{\"id\":\"123\"}},\"from\":{\"id\":\"3278\"},\"data\":\"TNG\"}}";
        upd = getUpdateGivenJson(entry);
        result = TrainScheduleTest.trainProcessor.process(upd);
        assertTrue(result);
        TrainScheduleTest.trainHandler.processMessage(upd);
    }

}
